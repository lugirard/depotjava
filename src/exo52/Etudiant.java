package exo52;

/**
 * @author lgirard
 */
public class Etudiant {

    static int nbTotalEtudiants = 0;
    String nom;
    static String IUT = "IUT de Bordeaux";

    Etudiant() {
        nbTotalEtudiants++;
    }

    Etudiant(String nom) {
        this.nom = nom;
        nbTotalEtudiants++;
    }

    public static void main(String[] args) {
        System.out.println("Au début : nb d'étudiants = " + Etudiant.nbTotalEtudiants);
        //acces à Etudiant.IUT via des instances...
        Etudiant bob = new Etudiant("Bob");
        System.out.println("Après bob : nb d'étudiants = " + Etudiant.nbTotalEtudiants);
        Etudiant greg = new Etudiant("Greg");
        System.out.println("Après Greg : nb d'étudiants = " + Etudiant.nbTotalEtudiants);
        Etudiant robert = new Etudiant();
        System.out.println("Après robert : nb d'étudiants = " + Etudiant.nbTotalEtudiants);
    }
}
